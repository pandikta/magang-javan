<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalkulaktorController extends Controller
{
    public function index()
    {
        return view('kalkulator');
    }

    public function hitung(Request $request)
    {
        $operator = $request->input('operator');
        $angka1 = $request->input('angka1');
        $angka2 = $request->input('angka2');
        if (!is_numeric($angka1) && !is_numeric($angka2)) {
            $hasil = "Harus Angka";
        } else {
            if ($operator == '+') {
                $hasil =  $angka1 + $angka2;
            } elseif ($operator == '-') {
                $hasil =  $angka1 - $angka2;
            } elseif ($operator == '/') {
                if ($angka2 == 0) {
                    $hasil =  'Tidak bisa dilakukan';
                } else {
                    $hasil =  $angka1 / $angka2;;
                }
            } else {
                $hasil =  $angka1 * $angka2;
            }
        }

        return view('kalkulator', ['hasil' => $hasil]);
    }
}
