<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <title>Tugas 2 kalkulator</title>
</head>

<body>
    <div class="container">
        <h1>kalkulator</h1>

        <form action="" method="post" class="row g-3">
            @csrf
            <div class="col-auto">
                <input type="text" name="angka1" class="form-control" id="angka1" placeholder="Masukkan Angka 1">
            </div>
            <div class="col-auto">
                <select class="form-select" name="operator" aria-label="Default select example">
                    <option value="+" selected>+</option>
                    <option value="-">-</option>
                    <option value="/">/</option>
                    <option value="*">*</option>
                </select>
            </div>
            <div class="col-auto">
                <input type="text" name="angka2" class="form-control" id="angka2" placeholder="Masukkan Angka 2">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-3">Hitung</button>
            </div>
        </form>
        @if (!empty($hasil))
            <h1> {{$hasil}}</h1>
        @endif
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous">
    </script>

</body>

</html>