<?php

use App\Http\Controllers\GanjilGenapController;
use App\Http\Controllers\KalkulaktorController;
use App\Http\Controllers\VokalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('/hello', 'helloworld');
Route::get('/ganjilgenap', [GanjilGenapController::class, 'index']);
Route::post('/ganjilgenap', [GanjilGenapController::class, 'operasi']);
Route::get('/kalkulator', [KalkulaktorController::class, 'index']);
Route::post('/kalkulator', [KalkulaktorController::class, 'hitung']);
Route::get('/vokal', [VokalController::class, 'index']);
Route::post('/vokal', [VokalController::class, 'vokal']);
