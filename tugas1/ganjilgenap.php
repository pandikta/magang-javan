<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <title>Tugas 1 Ganjil Genap</title>
</head>

<body>
    <div class="container">
        <h1>Ganjil Genap</h1>

        <form action="" method="post" class="row g-3">
            <div class="col-auto">
                <input type="text" name="angka1" class="form-control" id="angka1" placeholder="Masukkan Angka 1">
            </div>
            <div class="col-auto">
                <input type="text" name="angka2" class="form-control" id="angka2" placeholder="Masukkan Angka 2">
            </div>
            <div class="col-auto">
                <button type="submit" name="submit" class="btn btn-primary mb-3">Submit</button>
            </div>
        </form>


        <?php

        function cekgenapganjil($a, $b)
        {

            for ($i = $a; $i <= $b; $i++) {
                if ($i % 2 == 0) {
                    echo "$i Bilangan Genap <br>";
                } else {
                    echo  "$i Bilangan Ganjil <br>";
                }
            }
        }

        if (isset($_POST["submit"])) {
            $a = $_POST["angka1"];
            $b = $_POST["angka2"];
            cekgenapganjil($a, $b);
        }

        ?>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

</body>

</html>