<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <title>Tugas 1 kalkulator</title>
</head>

<body>
    <div class="container">
        <h1>kalkulator</h1>

        <form action="" method="post" class="row g-3">
            <div class="col-auto">
                <input type="text" name="hitung" class="form-control" id="angka1" placeholder="Masukkan perhitungan">
            </div>
            <div class="col-auto">
                <button type="submit" name="submit" class="btn btn-primary mb-3">Submit</button>
            </div>
        </form>


        <?php

        function kalkulator($hitung)
        {
            $arrAngka = str_replace(' ', '', $hitung);
            $angka1 = $arrAngka[0];
            $angka2 = $arrAngka[2];
            $operator = $arrAngka[1];
            if (!is_numeric($angka1) && !is_numeric($angka2)) {
                echo 'Harus Angka';
            } else {
                if ($operator == '+') {
                    echo $angka1 + $angka2;
                } elseif ($operator == '-') {
                    echo $angka1 - $angka2;
                } elseif ($operator == '/') {
                    if ($angka2 == 0) {
                        echo 'Tidak bisa dilakukan';
                    } else {
                        echo $angka1 / $angka2;
                    }
                } else {
                    echo $angka1 * $angka2;
                }
            }
        }

        if (isset($_POST["submit"])) {
            $hitung =  $_POST["hitung"];
            kalkulator($hitung);
        }

        ?>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

</body>

</html>