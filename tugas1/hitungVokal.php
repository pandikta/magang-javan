<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <title>Tugas 1 Hitung Huruf Vokal</title>
</head>

<body>
    <div class="container">
        <h1>Hitung Huruf Vokal</h1>

        <form action="" method="post" class="row g-3">
            <div class="col-auto">
                <input type="text" name="nama" class="form-control" id="angka1" placeholder="Masukkan nama anda">
            </div>
            <div class="col-auto">
                <button type="submit" name="submit" class="btn btn-primary mb-3">Submit</button>
            </div>
        </form>


        <?php

        function hitungVokal($nama)
        {
            $kata = strtolower($nama);
            if (is_numeric($kata)) {
                echo "tidak boleh angka";
            } elseif (empty($kata)) {
                echo "tidak boleh kosong";
            } else {

                $vokal = ['a', 'i', 'u', 'e', 'o'];
                $arrKata = str_split($kata);

                $cek = array_intersect($arrKata, $vokal);
                $cek = array_unique($cek);
                $string = implode($cek);
                $pecah_string = str_split($string);
                if (strlen($string) == 5) {
                    echo $kata . " = " . count($cek) . " yaitu " . $pecah_string[0] . " , " . $pecah_string[1] . " , " . $pecah_string[2] . " , " . $pecah_string[3] . " dan " . $pecah_string[4];
                } elseif (strlen($string) == 4) {
                    echo $kata . " = " . count($cek) . " yaitu " . $pecah_string[0] . " , " . $pecah_string[1] . " , " . $pecah_string[2] . " dan " . $pecah_string[3];
                } elseif (strlen($string) == 3) {
                    echo $kata . " = " . count($cek) . " yaitu " . $pecah_string[0] . " , " . $pecah_string[1] . " dan " . $pecah_string[2];
                } elseif (strlen($string) == 2) {
                    echo $kata . " = " . count($cek) . " yaitu " . $pecah_string[0] . " dan " . $pecah_string[1];
                } elseif (strlen($string) == 1) {
                    echo $kata . " = " . count($cek) . " yaitu hanya " . $pecah_string[0];
                } else {
                    echo "tidak ada huruf vokal";
                }
            }
        }

        if (isset($_POST['submit'])) {
            $nama = $_POST["nama"];
            hitungVokal($nama);
        }

        ?>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

</body>

</html>